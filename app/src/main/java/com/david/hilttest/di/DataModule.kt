package com.david.hilttest.di

import com.david.hilttest.api.CallOfDutyApi
import com.david.hilttest.data.DataRepositoryImpl
import com.david.hilttest.domain.DataRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

/**
    @Module se usa para especificar que es un modulo de hilt
    @InstallIn Se usa para instalar un modulo en un componente de Hilt
    @Provides Se usa para inyectar instancias que no son de mi propiedad
    @Binds Se usa para inyecyar instancias que no se pueden intectar con un constructor
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
object DataModule {
    @Provides
    fun provideDataRepository(
        callOfDutyApi: CallOfDutyApi
    ): DataRepository = DataRepositoryImpl(callOfDutyApi)
}
