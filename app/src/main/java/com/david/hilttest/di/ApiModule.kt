package com.david.hilttest.di

import com.david.hilttest.api.ApiController
import com.david.hilttest.api.CallOfDutyApi
import com.david.hilttest.data.DataRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

//Se inyecta como single a nivel del application
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = ApiController.getApiController()

    @Singleton
    @Provides
    fun provideApiController(retrofit: Retrofit): CallOfDutyApi =
        retrofit.create(CallOfDutyApi::class.java)
}