package com.david.hilttest.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiController {
    fun getApiController(): Retrofit {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val request = original.newBuilder()
                        .addHeader(
                            "x-rapidapi-key",
                            "045951b7f7mshc4e2896a3f942bbp18aa6ejsn0635495b95d5"
                        )
                        .method(original.method, original.body)
                        .build()

                    return chain.proceed(request)
                }
            })

        return Retrofit.Builder()
            .client(httpClient.build())
            .baseUrl("https://call-of-duty-modern-warfare.p.rapidapi.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}