package com.david.hilttest.api

import com.david.hilttest.data.models.Multiplayer
import retrofit2.http.GET
import retrofit2.http.Path

interface CallOfDutyApi {

    @GET("/multiplayer/{username}/{platform}")
    suspend fun getMultiplayer(
        @Path("username") userName: String,
        @Path("platform") platform: String
    ): Multiplayer
}