package com.david.hilttest.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.lifecycleScope
import com.david.hilttest.R
import com.david.hilttest.data.models.Multiplayer
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
Una vez que se configura Hilt en tu clase Application y hay un componente disponible en el nivel de la aplicación,
Hilt puede proporcionar dependencias para otras clases de Android que tengan la anotación @AndroidEntryPoint
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val dataViewModel: DataViewModel by viewModels()

    private val edtUserName by lazy<EditText> { findViewById(R.id.edtUserName) }
    private val edtPlatform by lazy<EditText> { findViewById(R.id.edtPlatform) }
    private val btnGetMultiplayer by lazy<AppCompatButton> { findViewById(R.id.btnGetMultiplayer) }
    private val txvUserData by lazy<AppCompatTextView> { findViewById(R.id.txvUserData) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initListeners()
        initSubscription()
    }

    private fun initListeners() {
        btnGetMultiplayer.setOnClickListener {
            dataViewModel.getMultiplayer(
                edtUserName.text.toString(),
                edtPlatform.text.toString()
            )
        }
    }

    private fun initSubscription() {
        dataViewModel.multiplayer.observe(this, {
            when (it) {
                is MultiplayerState.Success -> {
                    processData(it.data)
                }
                is MultiplayerState.Error -> {
                    txvUserData.text = "Surgio un error"
                }
            }
        })
    }

    private fun processData(data: Multiplayer) {
        txvUserData.text = """
        Nombre de usuario: ${data.userName}
        Plataforma: ${data.platform}
        Nivel: ${data.level}
        Prestigio: ${data.prestige}
        Exp Total: ${data.totalXp}
        """
    }
}