package com.david.hilttest.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.david.hilttest.domain.DataUC
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
Un ViewModel de Hilt es un ViewModel de Jetpack que es inyectado por el constructor por Hilt.
Para habilitar la inyección de un ViewModel por Hilt, use la anotación @HiltViewModel:
 */
@HiltViewModel
class DataViewModel @Inject constructor(
    private val dataUC: DataUC
) : ViewModel() {

    private val _multiplayer = MutableLiveData<MultiplayerState>()

    val multiplayer: LiveData<MultiplayerState>
        get() = _multiplayer

    fun getMultiplayer(userName: String, platform: String) {
        viewModelScope.launch {
            dataUC.getMultiplayer(userName, platform).collect {
                _multiplayer.postValue(MultiplayerState.Success(it))
            }
        }
    }
}