package com.david.hilttest.presentation

import com.david.hilttest.data.models.Multiplayer

sealed class MultiplayerState {
    data class Success(val data: Multiplayer) : MultiplayerState()
    object Error : MultiplayerState()
}