package com.david.hilttest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
@HiltAndroidApp activa la generación de código de Hilt, incluida una clase base para tu aplicación
que sirve como contenedor de dependencia en el nivel de la aplicación.
*/
@HiltAndroidApp
class AppController: Application() {
    override fun onCreate() {
        super.onCreate()
    }
}
