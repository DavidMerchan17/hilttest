package com.david.hilttest.data

import com.david.hilttest.api.CallOfDutyApi
import com.david.hilttest.data.models.Multiplayer
import com.david.hilttest.domain.DataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DataRepositoryImpl @Inject constructor(
    private val callOfDutyApi: CallOfDutyApi
) : DataRepository {
    override fun getMultiplayer(
        userName: String,
        platform: String
    ): Flow<Multiplayer> = flow {
        val response = callOfDutyApi.getMultiplayer(
            userName,
            platform
        )
        emit(response)
    }
}