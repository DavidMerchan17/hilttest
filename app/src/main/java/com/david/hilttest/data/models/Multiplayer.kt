package com.david.hilttest.data.models

import com.google.gson.annotations.SerializedName

data class Multiplayer(
    @SerializedName("platform")
    val platform: String,
    @SerializedName("username")
    val userName: String,
    @SerializedName("level")
    val level: Int,
    @SerializedName("prestige")
    val prestige: Int,
    @SerializedName("totalXp")
    val totalXp: Long,
)
