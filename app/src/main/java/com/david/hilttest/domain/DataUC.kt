package com.david.hilttest.domain

import com.david.hilttest.data.models.Multiplayer
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DataUC @Inject constructor(
    private val dataRepository: DataRepository
) {
    fun getMultiplayer(
        userName: String,
        platform: String
    ): Flow<Multiplayer> = dataRepository.getMultiplayer(
        userName,
        platform
    ).catch {
        println("Error: ${it.localizedMessage}")
    }
}