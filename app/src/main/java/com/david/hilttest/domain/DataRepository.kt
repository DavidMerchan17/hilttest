package com.david.hilttest.domain

import com.david.hilttest.data.models.Multiplayer
import kotlinx.coroutines.flow.Flow

interface DataRepository {
    fun getMultiplayer(userName: String, platform: String): Flow<Multiplayer>
}